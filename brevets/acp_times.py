"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """

    a = arrow.get(brevet_start_time)
    
    open_time = 0
    count = 0
    
    while (count + 200 <= control_dist_km):
        if (count + 200 <= 200):
            open_time += 200/34
            count += 200
        elif (count + 200 <= 400):
            open_time += 200/32
            count += 200

        elif (count + 200 <= 600):
            open_time += 200/30
            count += 200

        elif (count + 200 <= 1000):
            open_time += 200/28
            count += 200
            
        elif (count + 200 <= 1300):
            open_time += 200/26
            count += 200

    final_val = control_dist_km - count
    if (control_dist_km <= 200):

        open_time += final_val/34

    elif (control_dist_km <= 400):
        
        open_time += final_val/32

    elif (control_dist_km <= 600):
        open_time += final_val/30
        
    elif (control_dist_km <= 1000):
        open_time += final_val/28

    elif (control_dist_km <= 1300):
        open_time += final_val/26

    new_time = a.shift(hours =+ open_time)
    if new_time.second > 30:
        new_time = new_time.shift(minutes =+ 1)
    
    return new_time.isoformat() 


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    a = arrow.get(brevet_start_time)
    
    close_time = 0
    count = 0

    if (control_dist_km) == 0:
        new_time = a.shift(hours =+ 1)
        return new_time.isoformat()
    
    while (count + 200 <= control_dist_km):
        if (count + 200 <= 200):
            close_time += 200/15
            count += 200
        elif (count + 200 <= 400):
            close_time += 200/15
            count += 200

        elif (count + 200 <= 600):
            close_time += 200/15
            count += 200

        elif (count + 200 <= 1000):
            close_time += 200/11.428
            count += 200
            
        elif (count + 200 <= 1300):
            close_time += 200/13.333
            count += 200

    final_val = control_dist_km - count
    if (control_dist_km <= 200):
               
        close_time += final_val/15

    elif (control_dist_km <= 400):

        close_time += final_val/15

    elif (control_dist_km <= 600):
        close_time += final_val/15
        
    elif (control_dist_km <= 1000):
        close_time += final_val/11.428

    elif (control_dist_km <= 1300):
        close_time += final_val/13.333

    new_time = a.shift(hours =+ close_time)
    if new_time.second > 30:
        new_time = new_time.shift(minutes =+ 1)
    return new_time.isoformat() 

        
