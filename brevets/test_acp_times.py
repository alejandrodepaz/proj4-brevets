from acp_times import open_time, close_time
import nose

def test_close_zero_control():

    assert close_time(0, 200, "2017-01-01 00:00") == "2017-01-01T01:00:00+00:00"

def test_open_100_control():

    assert open_time(100, 200, "2017-01-01 00:00") == "2017-01-01T02:56:28.235294+00:00"

def test_close_100_control():

    assert close_time(100, 200, "2017-01-01 00:00") == "2017-01-01T06:40:00+00:00"
    
def test_close_50_control():

    assert close_time(50, 200, "2017-01-01 00:00") == "2017-01-01T03:20:00+00:00"
 
def test_open_50_control():

    assert open_time(50, 200, "2017-01-01 00:00") == "2017-01-01T01:28:14.117647+00:00"
        
