Author: Alejandro De Paz 
Contact: adepaz@uoregon.edu

This ACP calculator utilizes python in the backend and AJAX in the 
frontend to provide a user interface that allows bikers to correctly
compute open/close control checkpoint times based on user-specified 
control distances and an overall brevet distance. The logic in the 
backend follows ACP standard protocol procedures, computing the open 
and close times for controls based on the following criterion:


Control location (km): 0-200, 200 - 400, 400 - 600, 600 - 1000, 1000 - 1300	

Minimum Speed (km/hr): 15, 15, 15, 11.428, 13.333

Maximum Speed (km/hr): 34, 32, 30, 28, 26


Opening and closing times for intermediate checkpoints are determined by adding minimum and 
maximum travel times to the start checkpoint's opening time. Travel times are calculated by 
dividing the total distance of the checkpoint from the start by a minimum and a maximum speed. 
Minimum and maximum speeds vary according to total distance. 

Example: Consider a control at 350 km. The calculation is as follows:
200/34 + 150/32 = 5H53 + 4H41 = 10H34

The 200/34 gives us the minimum time to complete the first 200km while the 150/32 gives us the 
minimum time to complete the next 150km. The sum gives us the control's opening time.


A few edge cases to note:

i) Per ACP rules, the maximum finish times for each possible finish control 
(or total brevet distance) are:

13:30 for 200 KM, 20:00 for 300 KM, 27:00 for 400 KM, 40:00 for 600 KM, and 75:00
for 1000 KM.

ii) If a control is given that is more than 20% larger than the total brevet 
distance, the algorithm will compute the open/close times using the exact 
total brevet distance, per ACP regulation. 

iii) The closing time for the start checkpoint is 1 hour after the opening time 
(time X).  

For more information, visit: https://rusa.org/pages/acp-brevet-control-times-calculator